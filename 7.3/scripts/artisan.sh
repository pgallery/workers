#!/bin/bash

set -e

su -s /bin/bash - www-data -c "cd /var/www/gallery \
    && php artisan $@"

